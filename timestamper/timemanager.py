from datetime import datetime as dt

def getCurrentTime():
    return dt.now().replace(microsecond=0)

class TimeManager():
    date = None
    start_time = None    
    end_time = None
    difference = None

    paused = False
    pause = False
    pause_time_start = None
    pause_time_end = None
    pause_difference = getCurrentTime()-getCurrentTime()

    # def __init__(self):
    #     self.start_time = time.strftime('%H:%M:%S')

    def logStartTime(self):
        return "startTime: " + self.start_time.strftime('%H:%M:%S')

    def logEndTimer(self):
        return "endTime: " + self.end_time.strftime('%H:%M:%S')

    def logDifference(self):
        return "difference: " + str(self.difference)

    def logPauseDifference(self):
        return "pause difference: " + str(self.pause_difference)

    def startTimer(self):
        self.date = dt.now().date(); 
        if self.pause == True:
            self.pause_time_end = getCurrentTime()
            self.pause_difference = self.pause_time_end - self.pause_time_start
            self.pause = False
        else:
            self.start_time = getCurrentTime()

    def pauseTimer(self):
        self.paused = True
        self.pause = True
        self.pause_time_start = getCurrentTime()
    

    def endTimer(self):
        if self.pause == True:
            self.end_time = self.pause_time_start
            self.paused = False            
            self.pause = False
        else:
            self.end_time = getCurrentTime()
        
        self.difference = self.end_time - self.start_time
        if self.paused == True:
            self.difference = self.difference - self.pause_difference

    
    