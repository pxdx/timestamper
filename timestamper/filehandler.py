import os
import openpyxl as xl
from config import Config
from datetime import datetime as dt

from timemanager import TimeManager

def log(str):
    print("FileHandler: " + str)

class DataRow():
    def __init__(self, timemanager: TimeManager, desc: str):
        self.date = str(timemanager.date)
        self.start = timemanager.start_time.strftime('%H:%M:%S')
        self.end = timemanager.end_time.strftime('%H:%M:%S')
        self.pause = str(timemanager.pause_difference)
        self.diff = str(timemanager.difference)
        self.desc = desc
    
    def toCSV(self) -> str:
        return self.date + ";" + \
               self.start + ";" + \
               self.end + ";" + \
               self.pause + ";" + \
               self.diff + ";" + \
               self.desc + "\n"
        

class XlsxFileHandler():
    path = ''

    def __init__(self, path):
        self.path = path

        if os.path.isfile(self.path):
            self.wb = xl.load_workbook(self.path)
        else:
            self.wb = xl.Workbook()
            self.new = True
            

        if self.wb.active.title != self.current_month:
            self.createNewSheet()
    
    def createNewSheet(self):
        self.wb.create_sheet(self.current_month.upper())
        self.ws = self.wb.active
        self.ws.append(['date', 'start', 'end', 'description'])

    def save(self):
        log("saveWb")
        self.wb.save(self.path)

    def addTimestamp(self, date, start, end, pause, diff, desc):
        self.ws.append([str(date), start, end, pause, diff, desc])

class CsvFileHandler():
    path = ''
    file = None
    headline = 'date;start;end;pause;diff;desc\n'
    
    def __init__(self, path: str):
        self.path = path

    def add(self, data: DataRow) -> None:        
        if not os.path.isfile(self.path):
            f = open(self.path, 'a')
            f.write(self.headline)
            f.write(data.toCSV())
            f.close()
        else:
            f = open(self.path, 'a')            
            f.write(data.toCSV())
            f.close()
    
    def save(self) -> None:
        print("saved")

class FileHandler():
    wb = None
    ws = None
    path = None
    current_month = None
    new = False
    fh = None
    config = None

    def __init__(self, config: Config):
        self.config = config
        self.path = config.path + '/' + config.file_name + '.' + config.extension
        log("path " + self.path)
        self.current_month = dt.now().strftime("%B")
        
        if config.extension == "xlsx":
            self.fh = XlsxFileHandler(self.path)
        elif config.extension == "csv":
            self.fh = CsvFileHandler(self.path)
 
    def addTimestamp(self, data: DataRow):
        self.fh.add(data)
    
    def save(self):
        self.fh.save()
    

