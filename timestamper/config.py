import os
import json

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

PATH = "path"
FILE_NAME = "file_name"
EXTENSTION = "extension"
CONFIG_PATH = ROOT_DIR + "/config.json"

class Config():    
    def __init__(self):
        f = open(CONFIG_PATH)
        data = json.load(f)

        self._path = data[PATH]
        self._file_name = data[FILE_NAME]
        self._extension = data[EXTENSTION]

    def save(self):
        data = {
            PATH: self._path,
            FILE_NAME: self._file_name,
            EXTENSTION: self._extension
        }
        json_obj = json.dumps(data)
        with open(CONFIG_PATH, "w") as outfile:
            outfile.write(json_obj)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @path.deleter
    def path(self):
        del self._path

    @property
    def file_name(self):
        return self._file_name

    @file_name.setter
    def file_name(self, value):
        self._file_name = value

    @file_name.deleter
    def file_name(self):
        del self._file_name

    @property
    def extension(self):
        return self._extension

    @extension.setter
    def extension(self, value):
        self._extension = value

    @extension.deleter
    def extension(self):
        del self._extension




