import tkinter as tk

class Color():
    GREY = "grey"
    GREEN = "green"
    RED = "RED"
    BLUE = "BLUE"

class State():
        NORMAL = "normal"
        ACTIVE = "active"
        DISABLED = "disabled"

class Button():
    button = None
    state = None
    color = None
    def __init__(
        self, master, text, command, row, column, columnspan=1, color=Color.GREY, disabled=False,
        
    ):
        self.color = color
        self.button = tk.Button(
            master=master,
            command=command,
            text=text.upper(),
            width=10,
            height=1,
            bg=color,                            
        )
        self.button.grid(row=row, column=column, columnspan=columnspan)
        
        if disabled == True:
            self.disable()
        else:
            self.enable()

    def setColor(self, color=None):
        if color == None:
            self.button["bg"] = self.color 
        else:
            self.button["bg"] = color      
    
    def disable(self):
        self.state = State.DISABLED
        self.button["state"] = State.DISABLED
        self.setColor(Color.GREY)
    
    def enable(self):
        self.state = State.NORMAL
        self.button["state"] = State.NORMAL
        self.setColor()
