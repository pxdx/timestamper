import button as btn
from config import Config, ROOT_DIR
from filehandler import DataRow, FileHandler
from timemanager import TimeManager
import tkinter as tk
from tkinter import messagebox

import time

class App():

    clock_text = "current time: "
    clock = None
    current_time = time.strftime('%H:%M:%S')
    window = None

    started = False
    paused = False

    btn_start = None
    btn_pause = None
    btn_end = None

    logFrame = None

    desc = ""

    def __init__(self):
        self.config = Config()
        self.filehandler = FileHandler(self.config)
        self.timemanager = TimeManager()

        self.window = tk.Tk()
        self.window.attributes('-topmost', True)
        self.window.title("TIMESTAMPer")
        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)

        main_frame = tk.Frame(master=self.window)
        main_frame.pack(fill=tk.BOTH, expand=True)
        
        # window.tk.call("source", ROOT_DIR + "/azure.tcl")     
        # window.tk.call("set_theme", "light")

        self.window.geometry("340x140")

        self.window.grid_rowconfigure(1, weight=2)
        self.window.grid_rowconfigure(2, weight=0)

        text_frame = tk.Frame(master=main_frame)
        text_frame.grid(row=0)
        
        self.createTitle(master=text_frame, row=0, column=0)        
        self.createTimer(master=text_frame, row=0, column=1)
        
        self.createMenu(master=main_frame, row=1, column=0, columnspan=2)        
        self.createLogFrame(master=main_frame, row=2, column=0, columnspan=2)
        self.window.mainloop()

   
    def tick(self):                
        new_time = time.strftime('%H:%M:%S')
        if new_time != self.current_time:
            self.current_time = new_time
            self.clock.config(text=self.clock_text + new_time)
        
        self.clock.after(200, self.tick)

    def on_closing(self):
        # if messagebox.askokcancel("Quit", "Do you want to quit?"):
        self.filehandler.save()
        self.window.destroy()

    def createTitle(self, master, row, column, columnspan=1):
        title = tk.Label(master=master, text="Welcome to my timestamper")
        title.grid(row=row, column=column, columnspan=columnspan)       


    def createTimer(self, master, row, column, columnspan=1):
        timer_frame = tk.Frame(master=master)
        timer_frame.grid(row=row, column=column, columnspan=columnspan)
        self.clock = tk.Label(master=timer_frame, text=self.clock_text + self.current_time)
        
        self.clock.pack(fill=tk.BOTH, expand=1)
        self.tick()
        
    def createLogFrame(self, master, row, column, columnspan=1):
        self.logFrame = tk.Text(master=master, width=40, height=5)
        self.logFrame.grid(row=row, column=column, columnspan=columnspan)        


    def addLog(self, str):
        self.logFrame.insert(tk.END, time.strftime('%H:%M:%S') + ' : ' + str + "\n")
        self.logFrame.see(tk.END)

    def createMenu(self, master, row, column, columnspan=1):
        menu = tk.Frame(master=master)    
        menu.grid(row=row, column=column, columnspan=columnspan)   

        self.btn_start = btn.Button(
            master=menu,
            text="start", color=btn.Color.GREEN,
            row=1, column=1,
            command=self.handleStartClick,
        )

        self.btn_pause = btn.Button(
            master=menu,
            text="pause", color=btn.Color.BLUE, disabled=True,
            row=1, column=2,
            command=self.handlePauseClick,
        )
                
        self.btn_end = btn.Button(
            master=menu,
            text="end", color=btn.Color.RED, disabled=True,
            row=1, column=3,
            command=self.handleEndClick,
        )

    def handleStartClick(self):
        self.started = True
        self.btn_start.disable()
        self.btn_pause.enable()
        self.btn_end.enable()        
        self.timemanager.startTimer()
        self.addLog(self.timemanager.logStartTime())
        
    def handlePauseClick(self):
        self.paused = True
        self.btn_start.enable()
        self.btn_pause.disable()
        self.addLog("pause clicked\n")
        self.timemanager.pauseTimer()
    
    def evaluate(self, event):
        self.desc = self.newWindow_enter.get()
        self.addLog("description:  " + self.desc)
        self.newWindow.destroy()
        data = DataRow(self.timemanager, self.desc)
        self.filehandler.addTimestamp(data)

    def openNewWindow(self):
     
        self.newWindow = tk.Toplevel(self.window)
        self.newWindow.attributes('-topmost', True)        
    
        self.newWindow.title("Enter Description")
        self.newWindow.geometry("260x40")
        
        label = tk.Label(self.newWindow, text ="Enter short Description")   
        label.grid(row=1, column=0)

        self.newWindow_enter = tk.Entry(master=self.newWindow)
        self.newWindow_enter.grid(row=1, column=1)
        self.newWindow_enter.focus()
        self.newWindow_enter.bind("<Return>", self.evaluate)
        


    def handleEndClick(self):
        self.started = False
        self.btn_start.enable()
        self.btn_pause.disable()
        self.btn_end.disable()        
        self.timemanager.endTimer()
        self.addLog(self.timemanager.logEndTimer())
        self.addLog(self.timemanager.logDifference())
        self.addLog(self.timemanager.logPauseDifference())
        self.openNewWindow()

    
   